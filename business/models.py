from django.db import models
from django.db.models import Count

from posts.models import Post, MyUser, Location


class Business(Post):
    def get_absolute_url(self):
        return "/business/{}/".format(self.id)
    def is_business(self):
        return True

class RateBusines(models.Model):
	user = models.ForeignKey(MyUser, related_name="user_rating")
	business = models.ForeignKey(Business, related_name="business_rating")
	rate = models.FloatField()
