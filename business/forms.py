from crispy_forms.layout import Field
from django import forms
from django.contrib.auth import get_user_model
from posts.models import Location
from business.models import Business


class LocationModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name

STATUS_CHOICES = (
    ('single', 'Going as Single'),
    ('taken', 'Going with a Partner'),
    ('confused', 'Still not sure')

)


class SingleOrTaken(forms.Form):
    following_type = forms.CharField(label="Status", widget=forms.RadioSelect(choices=STATUS_CHOICES))


class BusinessForm(forms.ModelForm):
    class Meta:
        model = Business
        fields = [
            "title",
            "content",
            "image",
            "location",
        ]

    location = LocationModelChoiceField(queryset=Location.objects.all())


class ProfileForm(forms.Form):
    location = LocationModelChoiceField(queryset=Location.objects.all())
