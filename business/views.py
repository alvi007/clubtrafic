import datetime
import googlemaps
from braces.views import JsonRequestResponseMixin, CsrfExemptMixin, LoginRequiredMixin
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count
from django.http import HttpResponseForbidden, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from googlemaps.exceptions import ApiError
from googlemaps.exceptions import _RetriableRequest
from django.views.decorators.csrf import csrf_exempt
from business.forms import BusinessForm
from posts.forms import SingleOrTaken
from posts.models import MyUser, Location
from .models import Business
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
try:
    import json
except ImportError:
    import simplejson as json


class Follow(CsrfExemptMixin, JsonRequestResponseMixin, LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if kwargs['post_id']:
            try:
                post = Business.objects.get(pk=kwargs['post_id'])
            except ObjectDoesNotExist:
                return self.render_bad_request_response({'message': 'Post does not exist'})
            user = MyUser.objects.get(pk=request.user.id)
            if user.following == post:
                return self.render_bad_request_response({'message': 'Already following that post'})
            else:
                user.following = post
                user.save()
                updated_post = Business.objects.filter(pk=kwargs['post_id']).annotate(
                    followers_count=Count('myuser')).get()
                return self.render_json_response({'followers_count': updated_post.followers_count})
        else:
            return self.render_bad_request_response({'message': 'Post id not provided'})


class TopBusiness(JsonRequestResponseMixin,LoginRequiredMixin,View):
    def get(self,request,*args,**kwargs):
        business_array = []
        businesses = Business.objects.filter(city=request.user.location).annotate(
        followers_count=Count('myuser')
        ).order_by(
            '-followers_count'
        )[:20]
        for business in businesses:
            business_array.append({
                'title': business.title,
                'count': business.followers_count
            })
        return self.render_json_response(business_array)
top_business = TopBusiness.as_view()


class PostsLocationsList(CsrfExemptMixin, JsonRequestResponseMixin, LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        post_array = []
        today = datetime.date.today()
        posts = Business.objects.filter(x_coordinate__isnull=False, y_coordinate__isnull=False)
        if posts.exists():
            for post in posts:
                post_array.append({
                    'longitude': post.x_coordinate,
                    'latitude': post.y_coordinate,
                    'id': post.id
                })
            return self.render_json_response(post_array)
        else:
            return self.render_bad_request_response({'message': 'Out of Business'})


class FollowType(CsrfExemptMixin, JsonRequestResponseMixin, LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            user = MyUser.objects.get(pk=request.user.id)
            user.following_type = request.POST['following_type']
            user.save()
            print user.following.type_count()
            return self.render_json_response({'following_type': user.following.type_count()})
        except (KeyError, ObjectDoesNotExist):
            return self.render_bad_request_response({'message': 'Invalid value'})


@login_required
def post_create(request):
    if not request.user.is_business:
        return HttpResponseForbidden()

    form = BusinessForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        instance = form.save(commit=False)
        try:
            g_maps = googlemaps.Client(settings.GOOGLE_API_KEY)
            city_name = instance.location.name
            location_api_json = g_maps.geocode('{},{}'.format(form.cleaned_data['address'], city_name))
            instance.x_coordinate = location_api_json[0]['geometry']['location']['lat']
            instance.y_coordinate = location_api_json[0]['geometry']['location']['lng']
        except (ValueError, _RetriableRequest, ApiError):
            pass
        instance.save()
    else:
        print(form.errors)

    context = {
        "form": form,
    }

    return render(request, "business/form.html", context)


@login_required
def post_detail(request, id):
    instance = get_object_or_404(Business, id=id)
    context = {
        "title": "Detail",
        "instance": instance,
    }
    return render(request, "business/detail.html", context)


@login_required
def post_list(request):
    single_or_taken_form = SingleOrTaken(request.POST or None)
    user_location = request.user.location
    queryset_list = Business.objects.filter(location=user_location).annotate(
        followers_count=Count('myuser')
    ).order_by(
        '-followers_count'
    )
    # queryset_list = Club.objects.filter(location=user_location)
    # Show 5 contacts per page
    paginator = Paginator(queryset_list, 10)

    page_request_variable = 'page'

    page = request.GET.get(page_request_variable)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)

    type_of_music = Business.objects.values_list('type_of_music', flat=True)

    arr_music = []
    for inf in type_of_music :
        List = {}
        List['name'] = inf
        arr_music.append(List)
    context = {
        "object_list": queryset,
        "title": "List",
        "page_request_var": page_request_variable,
        "single_or_taken_form": single_or_taken_form,
        "arr_music": arr_music
    }
    return render(request, "business/posts.html", context)


@login_required
def post_update(request):
    context = {
        "title": "Update"
    }
    return render(request, "business/posts.html", context)


@login_required
def post_delete(request):
    context = {
        "title": "Delete"
    }
    return render(request, "business/posts.html", context)

def demo(request):
    return render(request, "business/test.html")
