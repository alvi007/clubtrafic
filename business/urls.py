from django.conf.urls import url

from business.views import Follow, FollowType, PostsLocationsList, top_business, demo

urlpatterns = [
    url(r'^$', "business.views.post_list"),
    url(r'^create/$', "business.views.post_create"),
    url(r'^(?P<id>\d+)/$', "business.views.post_detail"),
    url(r'^update/$', "business.views.post_update"),
    url(r'^delete/$', "business.views.post_delete"),
    url(r'^follow/(?P<post_id>\d+)/$', Follow.as_view()),
    url(r'^follow/type/$', FollowType.as_view()),
    url(r'^locations/$', PostsLocationsList.as_view()),
    url(r'^top/$', top_business),
    url(r'^demo/$', demo),
]