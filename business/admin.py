from django.contrib import admin
from .models import Business, RateBusines
from posts.models import MyUser

# Register your models here.
class BusinessAdmin(admin.ModelAdmin):
	def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
		if db_field.name == 'user':
			kwargs["queryset"] = MyUser.objects.filter(is_business = True)
		return super(BusinessAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
		

admin.site.register(Business, BusinessAdmin)
admin.site.register(RateBusines)