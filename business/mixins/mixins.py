from django.http import HttpResponseForbidden


class BusinessAuthorization(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_business:
            return HttpResponseForbidden()
        return super(BusinessAuthorization, self).dispatch(request, *args, **kwargs)