from django.conf.urls import url

from member_location.views import location, updateCurrentPosition

urlpatterns = [
    url(r'^$', location),
    url(r'^updateCurrentPosition/$', updateCurrentPosition),
]