from __future__ import unicode_literals

from django.apps import AppConfig


class MemberLocationConfig(AppConfig):
    name = 'member_location'
