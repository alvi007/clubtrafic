from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from posts.models import MyUser
from business.models import Business
try:
    import json
except ImportError:
    import simplejson as json
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse


# Create your views here.
# @login_required
# def location(request):
# 	all_user = MyUser.objects.all()
# 	context = {
# 		'all_user' : all_user,
# 	}
# 	return render(request, "member_location/location.html", context)

@login_required
def location(request):
    all_user = MyUser.objects.all()
    all_business = Business.objects.all()
    context = {
        'all_user': all_user,
        'clubs': all_business
    }
    return render(request, "member_location/location.html", context)

@csrf_exempt
def updateCurrentPosition(request):
    pos = json.loads(request.POST.get("pos")) 
    user = MyUser.objects.get(id = request.user.id)
    user.lat_current = pos['lat']
    user.lng_current = pos['lng']
    user.save()
    all_user = MyUser.objects.all()
    all_business = Business.objects.all()
    data_response = []
    subData = {}
    data1 = []
    data2 = []
    for inf in all_user :
        list_info = {}
        list_info["lat"] = inf.lat_current 
        list_info["lng"] = inf.lng_current
        data1.append(list_info)

    for inf in all_business:
        list_info = {}
        list_info["lat"] = inf.x_coordinate 
        list_info["lng"] = inf.y_coordinate
        data2.append(list_info)

    subData['list_user'] =  data1
    subData['list_club'] = data2
    data_response.append(subData)
    print data_response
    return HttpResponse(json.dumps(data_response), content_type="application/json")
