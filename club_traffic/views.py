from django.shortcuts import render
from django.shortcuts import redirect
from posts.models import MyUser, Event
from business.models import Business


def home(request):
	if request.user.is_authenticated():
		all_user = MyUser.objects.all()
		all_business = Business.objects.all()
		all_event = Event.objects.all()
		context = {
			'all_user': all_user,
			'clubs': all_business,
			'events': all_event
		}
		return render(request, "member_location/location.html", context)
	else:
		return render(request, 'index.html')		
		 
