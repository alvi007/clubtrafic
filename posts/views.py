import datetime
from allauth import app_settings
from googlemaps import googlemaps
from allauth.account.utils import passthrough_next_redirect_url, complete_signup
from allauth.account.views import SignupView
from allauth.utils import get_request_param
from braces.views import JsonRequestResponseMixin, CsrfExemptMixin, LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic import View, FormView
from googlemaps.exceptions import _RetriableRequest, ApiError

from club_traffic import settings
from .models import Event, Location, MyUser, Post
from .forms import PostForm, ProfileForm, SingleOrTaken
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from business.models import Business, RateBusines
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from business.forms import BusinessForm
try:
    import json
except ImportError:
    import simplejson as json


class Follow(CsrfExemptMixin, JsonRequestResponseMixin, LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if kwargs['post_id']:
            try:
                post = Event.objects.get(pk=kwargs['post_id'])
            except ObjectDoesNotExist:
                return self.render_bad_request_response({'message': 'Post does not exist'})
            user = MyUser.objects.get(pk=request.user.id)
            if user.following == post:
                return self.render_bad_request_response({'message': 'Already following that post'})
            else:
                user.following = post
                user.save()
                updated_post = Event.objects.filter(pk=kwargs['post_id']).annotate(
                    followers_count=Count('myuser')).get()
                return self.render_json_response({'followers_count': updated_post.followers_count})
        else:
            return self.render_bad_request_response({'message': 'Post id not provided'})


class PostsLocationsList(CsrfExemptMixin, JsonRequestResponseMixin, LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        post_array = []
        today = datetime.date.today()
        posts = Event.objects.filter(x_coordinate__isnull=False, y_coordinate__isnull=False)
        if posts.exists():
            for post in posts:
                post_array.append({
                    'longitude': post.x_coordinate,
                    'latitude': post.y_coordinate,
                    'id': post.id
                })
            return self.render_json_response(post_array)
        else:
            return self.render_bad_request_response({'message': 'Out of Event'})


class FollowType(CsrfExemptMixin, JsonRequestResponseMixin, LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):

        try:
            user = MyUser.objects.get(pk=request.user.id)
            user.following_type = request.POST['following_type']
            user.save()
            return self.render_json_response({'following_type':user.following.type_count()})
        except (KeyError, ObjectDoesNotExist):
            return self.render_bad_request_response({'message': 'Invalid value'})


@login_required
def post_create(request):
    form = PostForm(request.POST or None, request.FILES or None)
    success = False
    duplicated = False
    methodPost = False
    if form.is_valid():
        methodPost = True
        try:
            event = Event.objects.get(title = form.cleaned_data['title'], location = form.cleaned_data['location'] )
            duplicated = True
        except Event.DoesNotExist :
            pass

        if duplicated == False :   
            instance = form.save(commit=False)
            try:
                g_maps = googlemaps.Client(settings.GOOGLE_API_KEY)
                city_name = instance.location.name
                location_api_json = g_maps.geocode('{},{}'.format(form.cleaned_data['address'], city_name))
                instance.x_coordinate = location_api_json[0]['geometry']['location']['lat']
                instance.y_coordinate = location_api_json[0]['geometry']['location']['lng']
            except (ValueError,_RetriableRequest,ApiError):
                pass
            instance.save()
            success = True
    else:
        print(form.errors)

    context = {
        "form": form,
        "success": success,
        "duplicated": duplicated,
        "methodPost": methodPost
    }

    return render(request, "post_form.html", context)


@login_required
def post_detail(request, id):
    instance = get_object_or_404(Event, id=id)
    context = {
        "title": "Detail",
        "instance": instance,
    }
    return render(request, "post_detail.html", context)


@login_required
def post_list(request):
    single_or_taken_form = SingleOrTaken(request.POST or None)
    user_location = request.user.location
    queryset_list = Event.objects.filter(instance_of=Post, location=user_location, ).annotate(
        followers_count=Count('myuser')
    ).order_by(
        '-followers_count'
    )
    # Show 5 contacts per page
    paginator = Paginator(queryset_list, 10)

    page_request_variable = 'page'

    page = request.GET.get(page_request_variable)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)
    context = {
        "object_list": queryset,
        "title": "List",
        "page_request_var": page_request_variable,
        "single_or_taken_form": single_or_taken_form
    }
    return render(request, "posts.html", context)


@login_required
def post_update(request):
    context = {
        "title": "Update"
    }
    return render(request, "posts.html", context)


@login_required
def post_delete(request):
    context = {
        "title": "Delete"
    }
    return render(request, "posts.html", context)




class ProfileEdit(FormView, LoginRequiredMixin):
    template_name = 'account/profile_edit.html'
    form_class = ProfileForm
    def get_initial(self):
        
        if self.request.user.is_business :
            return {
                'location': self.request.user.location,
                'first_name': self.request.user.first_name,
                'last_name': self.request.user.last_name,
                'following': self.request.user.following,
                'following_type': self.request.user.following_type,
                'business_name': self.request.user.business_name,
                'profile_photo': self.request.user.profile_photo,
                'sex': self.request.user.sex,
            }
        else :
            return {
                'location': self.request.user.location,
                'first_name': self.request.user.first_name,
                'last_name': self.request.user.last_name,
                'following': self.request.user.following,
                'following_type': self.request.user.following_type,
                'profile_photo': self.request.user.profile_photo,
                'sex' : self.request.user.sex,
                # 'business_name': self.request.user.business_name,
            }
   
    def form_valid(self, form):

        location = form.cleaned_data['location']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        following = form.cleaned_data['following']
        following_type = form.cleaned_data['following_type']
        profile_photo = form.cleaned_data['profile_photo']
        business_name = form.cleaned_data['business_name']
        sex = form.cleaned_data['sex']
        user = MyUser.objects.get(id=self.request.user.id)
        user.first_name = first_name
        user.last_name = last_name
        user.following = following
        user.following_type = following_type
        user.sex = sex
        user.profile_photo = profile_photo
        user.location = location
        user.business_name = business_name
        user.save()
        return HttpResponseRedirect(reverse('profile'))

class MySignupView(SignupView):

    def get_context_data(self, **kwargs):
        ret = super(MySignupView, self).get_context_data(**kwargs)
        form = ret['form']
        form.fields["email"].initial = self.request.session.get('account_verified_email', None)
        login_url = passthrough_next_redirect_url(self.request,
                                                  reverse("account_login"),
                                                  self.redirect_field_name)
        redirect_field_name = self.redirect_field_name
        redirect_field_value = get_request_param(self.request,
                                                 redirect_field_name)
        locations = Location.objects.all()
        ret.update({"login_url": login_url,
                    "redirect_field_name": redirect_field_name,
                    "redirect_field_value": redirect_field_value,
                    'locations': locations})
        return ret
my_signup = MySignupView.as_view()


@csrf_exempt
def getClub(request):
    idClub = request.POST.get('idClub')
    club = Business.objects.get(id=idClub)
    # array_result = serializers.serialize('json', [club], ensure_ascii=False)
    # print array_result
    # just_object_result = {}
    # just_object_result = array_result[1:-1]
    # print "hoang cong thinh"
    # print just_object_result
    array_result = []
    just_object_result = {}
    just_object_result['title'] = club.title
    just_object_result['address'] = club.address
    just_object_result['phone_number'] = club.phone_number
    just_object_result['hours_of_operation'] = club.hours_of_operation
    
    just_object_result['cover_price'] = club.cover_price
    just_object_result['dress_code'] = club.dress_code
    just_object_result['capacity'] = club.capacity
    just_object_result['age_restriction'] = club.age_restriction
    just_object_result['website'] = club.website
    just_object_result['type_of_music'] = club.type_of_music
    just_object_result['location'] = club.location.id
    just_object_result['content'] = club.content
    # print just_object_result
    array_result.append(just_object_result)
    return HttpResponse(json.dumps(array_result), content_type="application/json")

@csrf_exempt
def updateClub(request):
    data = request.POST
    idEditClub = data.get('idEditClub')
    name = data.get('title')
    address = data.get('address')
    phone_number = data.get('phone_number')
    hours_of_operation = data.get('hours_of_operation')
    cover_price = data.get('cover_price')
    dress_code = data.get('dress_code')
    capacity = data.get('capacity')
    age_restriction = data.get('age_restriction')
    website = data.get('website')
    type_of_music = data.get('type_of_music')
    location_id = data.get('location')
    content = data.get('content')
    lat = data.get('lat')
    lng = data.get('lng')
    location = Location.objects.get(id=location_id)
    club_id = None
    if idEditClub == "" :
        club = Business(location=location, x_coordinate = lat, y_coordinate = lng, user=request.user, title=name, address=address, phone_number=phone_number, hours_of_operation=hours_of_operation, cover_price=cover_price, dress_code=dress_code, capacity=capacity, age_restriction=age_restriction, website=website, type_of_music=type_of_music, content = content )
        club.save()
        club_id = club.id
    else :
        club = Business.objects.get(id = idEditClub)
        club.title = name
        club.address = address
        if lat != "" and lng != "" :
            club.x_coordinate = lat
            club.y_coordinate = lng
        club.phone_number = phone_number
        club.hours_of_operation = hours_of_operation
        club.cover_price = cover_price
        club.dress_code = dress_code
        club.capacity = capacity
        club.age_restriction = age_restriction
        club.website = website
        club.type_of_music = type_of_music
        club.location = location
        club.content = content
        club.save()
        club_id = club.id
    return HttpResponse(club_id)

@csrf_exempt
def deleteClub(request):
    idClub = request.POST.get("idClub")
    club = Business.objects.get(id=idClub)
    club.delete()
    return HttpResponse("success")

from django.db.models import Avg
@csrf_exempt
def rateBusiness(request):
    rate = request.POST.get("rate")
    business_id = request.POST.get("business_id")
    business = Business.objects.get(id=business_id)

    if RateBusines.objects.filter(user=request.user, business=business ).exists():
        pass
    else :
        rate_business = RateBusines(user=request.user, rate=rate, business=business)
        
        rate_business.save()
        avgRate = RateBusines.objects.filter( business=business ).aggregate(Avg('rate'))
        business.rate = avgRate['rate__avg']
        business.save()
        rate_percent = (business.rate*100)/5;
        return HttpResponse(rate_percent)
    return HttpResponse("rated")


def search(request):
    data = request.POST
    location_id = data.get('location')
    location = Location.objects.get(id=location_id)
    date = data.get('date')
    age_restriction = data.get('age_restriction')
    type_of_music = data.get('type_of_music')
    hours_of_operation = data.get('hours_of_operation')
    rate = data.get("rate")
    cover_price = data.get("cover_price")
    arr_date = date.split("/")
    date_django = ""
    if date != "" :
        date_django = arr_date[2] + "-"+ arr_date[0] + "-" + arr_date[1]
    single_or_taken_form = SingleOrTaken(request.POST or None)
    user_location = request.user.location
    queryset_list = None
    if data.get('ClubEvent') == 'clubbing' :
        # queryset_list = Business.objects.filter(cover_price__contains = cover_price, location=location, age_restriction__contains = age_restriction, type_of_music__contains = type_of_music, hours_of_operation__contains = hours_of_operation, rate__contains = rate).annotate(
        #     followers_count=Count('myuser')
        # ).order_by(
        #     '-followers_count'
        # )
        queryset_list = Business.objects.filter(cover_price__contains = cover_price, location=location, age_restriction__contains = age_restriction, type_of_music__contains = type_of_music, hours_of_operation__contains = hours_of_operation, rate__contains = rate).annotate(
            followers_count=Count('myuser')
        ).order_by(
            '-followers_count'
        )
    else :
        queryset_list = Event.objects.filter(location=location, date_party__contains = date_django, rate__contains = rate).annotate(
            followers_count=Count('myuser')
        ).order_by(
            '-followers_count'
        )
    # queryset_list = Club.objects.filter(location=user_location)
    # Show 5 contacts per page
    paginator = Paginator(queryset_list, 10)

    page_request_variable = 'page'

    page = request.GET.get(page_request_variable)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)

    type_of_music = Business.objects.values_list('type_of_music', flat=True)

    arr_music = []
    for inf in type_of_music :
        List = {}
        List['name'] = inf
        arr_music.append(List)
    sign_hide_validate = 1
    context = {
        "object_list": queryset,
        "title": "List",
        "page_request_var": page_request_variable,
        "single_or_taken_form": single_or_taken_form,
        'arr_music':arr_music,
        'sign_hide_validate':sign_hide_validate,
    }
    if data.get('ClubEvent') == 'clubbing' :
        return render(request, "business/posts.html", context)
    return render(request, "posts.html", context)