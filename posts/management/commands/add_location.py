from django.core.management.base import BaseCommand, CommandError
from posts.models import Location


class Command(BaseCommand):
    help = 'Add new location for events'

    def add_arguments(self, parser):
        parser.add_argument('location_name', nargs='+', type=str)

    def handle(self, *args, **options):
        for location_name in options['location_name']:
            location = Location(name=location_name).save()
            self.stdout.write(self.style.SUCCESS('Successfully added city {}'.format(location_name)))
