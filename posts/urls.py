from django.conf.urls import url

from posts.views import Follow, FollowType, PostsLocationsList
from . import views


urlpatterns = [
    url(r'^$', "posts.views.post_list"),
    url(r'^create/$', "posts.views.post_create"),
    url(r'^(?P<id>\d+)/$', "posts.views.post_detail"),
    url(r'^update/$', "posts.views.post_update"),
    url(r'^delete/$', "posts.views.post_delete"),
    url(r'^follow/(?P<post_id>\d+)/$', Follow.as_view()),
    url(r'^follow/type/$', FollowType.as_view()),
    url(r'^locations/$', PostsLocationsList.as_view()),
    url(r'^getClub/$', views.getClub),
    url(r'^updateClub/$', views.updateClub),
    url(r'^deleteClub/$', views.deleteClub),
    url(r'^rateBusiness/$', views.rateBusiness),
    url(r'^search/$', views.search),
    
]