from django.core.exceptions import ValidationError


def mail_domain_validator(value):
    if not ('@mail.usask.ca' in value or '@usask.ca' in value):
        raise ValidationError('Email domain is not allowed')
