from django.db.models import Count
from django.utils import timezone
from django.contrib.auth.models import AbstractUser, BaseUserManager
from polymorphic.models import PolymorphicModel

from posts.validators import mail_domain_validator
from django.db import models


class Post(PolymorphicModel):
    SELECT_AGE = (
        ('l', '18-22'),
        ('m', '23-27'),
        ('h', '28-32')
    )
    title = models.CharField(max_length=120)
    content = models.TextField()
    image = models.FileField(null=True, blank=True)
    location = models.ForeignKey('Location')
    x_coordinate = models.FloatField(null=True)
    y_coordinate = models.FloatField(null=True)
    address = models.CharField(max_length=255, null=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    rate = models.FloatField(null=True, default = 0 )
    phone_number = models.CharField(max_length = 30, null="True")
    hours_of_operation = models.IntegerField(null="True")
    cover_price = models.FloatField(null="True")
    dress_code = models.CharField(max_length = 255, null="True")
    capacity = models.IntegerField(null="True")
    # age_restriction = models.SmallIntegerField(null="True")
    age_restriction = models.CharField(choices=SELECT_AGE, max_length = 1, null=True)
    # age_restriction = models.
    website = models.URLField(null="True")
    type_of_music = models.CharField(max_length=255, null="True")
    user = models.ForeignKey('MyUser' , null=True, related_name="all_post")
    date_party = models.DateField(null = True)
    def get_absolute_url(self):
        return "/posts/{}/".format(self.id)

    def type_count(self):
        type_array = []
        followed_users_type_count = MyUser.objects.values(
            'following_type'
        ).filter(
            following=self
        ).annotate(
            count=Count('following_type')
        )
        single = 0
        taken = 0
        confused = 0

        for type_count in followed_users_type_count:
            if type_count['following_type'] == 'single':
                single = type_count['count']
            elif type_count['following_type'] == 'taken':
                taken = type_count['count']
            elif type_count['following_type'] == 'confused':
                confused = type_count['count']

        return_dict = {
            'single': single,
            'taken': taken,
            'confused': confused
        }
        return return_dict
    def is_business(self):
        return False
    def __unicode__(self):
        return u"%s"%self.title

class Event(Post):
    # club = models.ForeignKey('MyUser', related_name="all_event")
    pass



class MyUserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, username, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True, True,
                                 **extra_fields)


class MyUser(AbstractUser):
    objects = MyUserManager()

    FOLLOWING_TYPE_CHOICES = (
        ('single', 'single'),
        ('taken', 'taken'),
        ('confused', 'confused'),
    )
    SELECT_SEX = (
        ('ma', 'male'),
        ('fe', 'female')
    )
    class Meta:
        db_table = 'auth_user'

    def __init__(self, *args, **kwargs):
        AbstractUser.__init__(self, *args, **kwargs)

    profile_photo = models.ImageField(null=True, blank=True, upload_to="avatar")
    following = models.ForeignKey('Post', null=True, blank=True)
    location = models.ForeignKey('Location', null=True)
    following_type = models.CharField(choices=FOLLOWING_TYPE_CHOICES, max_length=10, null=True, blank = True)
    is_business = models.BooleanField(default=False)
    business_name = models.CharField(max_length=255, null=True, blank = True)
    sex = models.CharField(choices=SELECT_SEX, max_length = 2, default = 'ma')
    lat_current = models.FloatField(null = True)
    lng_current = models.FloatField(null = True)
    def getListLocation(self):
        return Location.objects.all()
        
class Location(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return u'%s'%self.name