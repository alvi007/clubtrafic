from django.contrib import admin

# Register your models here.
from .models import Post, MyUser, Location, Event


class PostAdmin(admin.ModelAdmin):
    list_display = ["title", "updated", "timestamp"]
    list_display_links = ["updated"]
    list_filter = ["updated", "timestamp"]
    search_fields = ["title", "content"]
    class Meta:
        model = Post

class MyUserAdmin(admin.ModelAdmin):
	def save_model(self, request, obj, form, change):
		# Override this to set the password to the value in the field if it's
		# changed.
		if obj.pk:
			orig_obj = MyUser.objects.get(pk=obj.pk)
			if obj.password != orig_obj.password:
				obj.set_password(obj.password)
		else:
			obj.set_password(obj.password)
		obj.save()



admin.site.register(Post, PostAdmin)
admin.site.register(MyUser, MyUserAdmin)
admin.site.register(Location)
admin.site.register(Event)
