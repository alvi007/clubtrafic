from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django import forms
from django.contrib.auth import get_user_model
from .models import Event, Location, MyUser
from django.contrib.auth.forms import ReadOnlyPasswordHashField

class LocationModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name

STATUS_CHOICES = (
    ('single', 'Going as Single'),
    ('taken', 'Going with a Partner'),
    ('confused', 'Still not sure')

)


class SingleOrTaken(forms.Form):
    following_type = forms.CharField(label="Status", widget=forms.RadioSelect(choices=STATUS_CHOICES))


class PostForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            "title",
            "content",
            "image",
            "address",
            "location",
            "date_party"
        ]

    location = LocationModelChoiceField(queryset=Location.objects.all())


class MySignupForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'password' , 'is_business','first_name', 'last_name', 'email', 'location', 'business_name']

    def signup(self, user):
        user.save()

    def clean_email(self):
        validate_email(self.cleaned_data['email'])
        if MyUser.objects.filter(email=self.cleaned_data['email']).exists():
            raise ValidationError('Email is not unique')
        if not self.cleaned_data['is_business']:
            if not (('@mail.usask.ca' or '@usask.ca') in self.cleaned_data['email']):
                raise ValidationError('Email domain is not allowed')
        return self.cleaned_data['email']

    def clean_business_name(self):
        if not self.cleaned_data['is_business'] : 
            if self.cleaned_data['business_name'] != "" :
                raise ValidationError('User doesent have business_name')
        elif not self.cleaned_data['business_name']:
            raise ValidationError('Business name needs to be filled out')
        return self.cleaned_data['business_name']

    def clean_password2(self):
        # Check that the two password entries match
        password = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password and password2 and password != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(MySignupForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

    username = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30, render_value=False)), label=("Username"))
    first_name = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30, render_value=False)), label=("First Name"))
    last_name = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30, render_value=False)), label=("Last Name"))
    business_name = forms.CharField(label="Business name", required=False)
    password = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label=("Password"))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label="Password (again)")

   
# class ProfileForm(forms.ModelForm):
#     class Meta:
#         model = MyUser
#         fields = [
#             "first_name", 
#             "last_name",
#             "following",
#             "following_type",
#             "location",
#         ]
#     # location = LocationModelChoiceField(queryset=Location.objects.all())

class ProfileForm(forms.ModelForm):
    class Meta:
        model = MyUser
        fields = ('first_name', 'last_name', 'following', 'following_type', 'location', 'business_name', 'profile_photo', 'sex')
        first_name = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30, render_value=False)), label=("First Name"))
        last_name = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30, render_value=False)), label=("Last Name"))
        # business_name = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=30, render_value=False)), label=("Business Name"))
    def clean_first_name(self):
        # Check that the two password entries match
        first_name = self.cleaned_data.get("first_name")
        if first_name == "":
            raise ValidationError('First name needs to be filled out')
        return first_name

    def clean_last_name(self):
        # Check that the two password entries match
        last_name = self.cleaned_data.get("last_name")
        if last_name == "":
            raise ValidationError('Last name needs to be filled out')
        return last_name 

    # def clean_following_type(self):
    #     print "toi la type2322"
    #     following_type = self.cleaned_data.get("following_type")
    #     list_type = ["taken", "confused", "single"]
    #     if following_type not in list_type:
    #         raise ValidationError("this field is required")
    #     return following_type
    # def clean_business_name(self):
    #     # Check that the two password entries match
    #     business_name = self.cleaned_data.get("business_name")
    #     if business_name == "":
    #         raise ValidationError('Business name needs to be filled out')
    #     return business_name
