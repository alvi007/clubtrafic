# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-07-28 07:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0012_auto_20160727_0410'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='following_type',
            field=models.CharField(blank=True, choices=[(b'single', b'single'), (b'taken', b'taken'), (b'confused', b'confused')], max_length=10, null=True),
        ),
    ]
