PostChart = function (element_id, single, taken, confused,place) {

    // Definition variables
    this.element_id = element_id;
    this.single = single;
    this.taken = taken;
    this.confused = confused;
    this.place = place;

    this._init();
    return this
};

PostChart.prototype = {

    _init: function () {
        var self = this;

        // Grab the chart data
        self.canvas = document.getElementById('bar_chart' + self.element_id);
        self.form = document.getElementById('status_form'+self.element_id);
        self.status_button = document.getElementById('status_button'+self.element_id);
        self.ctx = document.getElementById('bar_chart'+self.element_id).getContext("2d");
        $(this.status_button).click(function(){
             $.post('/'+self.place+'/follow/type/',{following_type: $(self.form).find('input:radio:checked').val()}).done(function (data) {
                   self._update(data)
             })

        });
        self._draw();

    },
    _draw: function () {
        var self = this;

        var data = {
            labels: ["Single", "Taken", "Confused"],
            datasets: [
                {
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: "rgba(220,220,220,0.75)",
                    highlightStroke: "rgba(220,220,220,1)",
                    data: [self.single, self.taken, self.confused]
                }
            ]
        };

        self.chart = new Chart(self.ctx).Bar(data);
    },
    _update: function (data) {
        var self = this;
        self.chart.datasets[0].bars[0].value = data.following_type.single;
        self.chart.datasets[0].bars[1].value = data.following_type.taken;
        self.chart.datasets[0].bars[2].value = data.following_type.confused;
        self.chart.update();
    }


};




