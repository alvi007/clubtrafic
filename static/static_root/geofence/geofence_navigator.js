GeoFenceMonitor = function (options) {
    // Definition variables
    var default_settings = {
        location_list_url : '/posts/locations/',
        follow_url : '/posts/follow/',
    }
    this.settings = $.extend(default_settings, options);
    this.monitoring_positions = null;
    this.watchID = null;
    this._init();
    return this
};

GeoFenceMonitor.prototype = {

    _init: function () {
        var self = this;
        self._get_locations();

    },
    _get_locations: function () {
        var self = this;
        // get all the locations available to monitor
        $.get(self.settings.location_list_url).done(function (data) {
            self.monitoring_positions = data;
            self.watchID = navigator.geolocation.watchPosition(function(position){
                            self._success(position,self)
            }, self._error, {maximumAge:60000, timeout:5000, enableHighAccuracy:true});
        });

    },
    _success: function (data, self) {
        // pass through array of locations when location is updated
        $.each(self.monitoring_positions, function(key, position){
            var distance = self._haversine_distance(position.longitude, position.latitude, data.coords.longitude, data.coords.latitude);
            console.log(distance);
            // TODO whatever is necessary for user
            
            if( distance < 100){
                $.get(self.settings.follow_url+position.id+'/').done(function (data) {
                    console.log(data);
                });

                return false;
            }


        });
    },
    _error: function (error) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.");
                break;
        }
    },
    _haversine_distance: function (lon1, lat1, lon2, lat2) {
        var self = this;
        var R = 6371000; // km

        var x1 = lat2 - lat1;
        var dLat = self._to_rad(x1);
        var x2 = lon2 - lon1;
        var dLon = self._to_rad(x2);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(self._to_rad(lat1)) * Math.cos(self._to_rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    },
    _to_rad:function(x) {
            return x * Math.PI / 180;
    }
};


