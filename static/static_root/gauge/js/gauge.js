Gauge = function (element_id,count,followed,place) {

    // Definition variables
    this.element_id = element_id;
    this.follower_count = count;
    this.place = place;

    // interval change in miliseconds
    this.interval = 25;
    // interval angle step
    this.angleChange = 0.5;
    // maximum angle (x - 90)
    this.max_angle = 70;
    // starting angle
    this.angle = -90;
    if (followed=='true'){
        this.added_angle = true;
    }
    else {
        this.added_angle = false;
    }
    this.desired_angle = null;
    this.needle = null;
    this.img = null;
    this.ctx = null;
    this.canvas = null;
    this._init();
    return this
};

Gauge.prototype = {

    _init: function () {
        var self = this;

        // Grab the compass element
         self.canvas = document.getElementById('gauge'+self.element_id);
         self.button = document.getElementById('gauge_button'+self.element_id);
         self.form = document.getElementById('status_form'+self.element_id);

        if (this.added_angle){
            $(self.button).hide();
            $(self.form).show();
        }

        $(this.status_button).click(function(){
             $.post('/'+self.place+'/follow/type/',{following_type: $(self.form).find('input:radio:checked').val()}).done(function () {
                    // TODO user notification of some kind
             })

        });
        // Canvas supported?
        if (this.canvas.getContext('2d')) {
            self.ctx = this.canvas.getContext('2d');

            self.desired_angle = self._logConversion(self.follower_count);

            // Load the needle image
            self.needle = new Image();
            self.needle.src = '/static/static_root/gauge/img/needle.png';

            // Load the compass image
            self.img = new Image();
            self.img.src = '/static/static_root/gauge/img/gauge.png';
            self.img.onload = self.imgLoaded.bind(self);

            $(self.button).click(function () {

                $.get('/'+self.place+'/follow/'+self.element_id+'/').done(function () {
                    self.added_angle = true;
                    self.desired_angle = self.desired_angle + 2;
                    self.imgLoaded(self);
                    $(self.button).hide();
                    $(self.form).show();
                    $.each(gauge_collection, function(key,value){
                       if (value.element_id == self.element_id){
                       }
                        else {
                           value.removeFollowed(value)
                       }
                    });
                })
            })
        }
        else {
            alert("Canvas not supported!");
        }

    },
    _draw: function () {
            var self =this;
        if (self.angle <= self.max_angle && self.angle <= self.desired_angle) {
            self._clearCanvas();

            self.ctx.drawImage(self.img, 0, 0);

            self.ctx.save();

            self.ctx.translate(70, 12);

            self.ctx.translate(5, 60);

            self.ctx.rotate(self.angle * (Math.PI / 180));

            self.ctx.drawImage(self.needle, -5, -60);


            self.ctx.restore();

            self.angle += self.angleChange;
        }
        else if (self.angle  >= (self.desired_angle + self.angleChange)){
             self._clearCanvas();

            self.ctx.drawImage(self.img, 0, 0);

            self.ctx.save();

            self.ctx.translate(70, 12);

            self.ctx.translate(5, 60);

            self.ctx.rotate(self.angle * (Math.PI / 180));

            self.ctx.drawImage(self.needle, -5, -60);


            self.ctx.restore();

            self.angle -= self.angleChange;
        }
        else{
            self.angle -= Math.random()*2.2
        }
    },
    imgLoaded: function () {
        var self = this;
       setInterval(self._draw.bind(self), self.interval);
    },
    _clearCanvas: function () {
        var self = this;
        // clear canvas
        self.ctx.clearRect(0, 0, 150, 83);
    },
    _logConversion: function(value){
        // follow up formula
            var calculatedValue = 1 - Math.log(15)/Math.log(15+parseInt(value));
        return 180*calculatedValue - 90
    }


};

Gauge.prototype.removeFollowed = function(self) {
        if (self.added_angle){
            self.added_angle = false;
            self.desired_angle =  self.desired_angle -2;
            self.imgLoaded();
            $(self.button).show();
            $(self.form).hide();
        }
    };



