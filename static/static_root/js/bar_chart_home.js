PostBusinessChart = function () {

    // Definition dataset
    this.data = {
        labels: [],
        datasets: [
            {
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: []
            }
        ]
    };
    this._init();
    return this
};

PostBusinessChart.prototype = {

    _init: function () {
        var self = this;

        // Grab the chart data
        self.ctx = document.getElementById('bar_chart_business').getContext("2d");

        self._load();

    },
    _load: function(){
        var self = this;
        $.get('/business/top/').done(function (data) {
            $.each(data, function(key,value){
                self.data.labels.push(value.title);
                self.data.datasets[0].data.push(value.count);
            });
            self._draw();
        });
    },
    _draw: function () {
        var self = this;
        self.chart = new Chart(self.ctx).Bar(self.data);
    },
    _refresh: function () {
        var self = this;
        self._load()
    }


};