$('document').ready(function() {
	var idClub;
	$(".listClubs").on("click", ".btnEditClub", function() {
		idClub = $(this).parent().attr("idClub");
		$("#id_is_editClub").val(idClub);
		$(".errorField").addClass("displayNONE");
		$(".errorField").parent().addClass("displayNONE");
        var url = "/posts/getClub/";     
        $.ajax({
          type: "POST",
          url: url,
          data: {"idClub": idClub},
        }).done(function(response){
        	for (var key in response[0]) {
				$(".dataClub[name="+key+"]").val(response[0][key])
			}
        });
	})

	$(".dataClub").keyup(function() {
	    if($(this).val() == "") {
	        $("#"+$(this).attr("id_tmp")).text("this field is required");
	        $("#"+$(this).attr("id_tmp")).removeClass("displayNONE");
	        $("#"+$(this).attr("id_tmp")).parent().removeClass("displayNONE");
	    }
	    else {
	        $("#"+$(this).attr("id_tmp")).addClass("displayNONE");
	        $("#"+$(this).attr("id_tmp")).parent().addClass("displayNONE");
	    }
	}) 
	$("#btnAddClub").click(function() {
		$(".errorField").addClass("displayNONE");
		$(".errorField").parent().addClass("displayNONE");
		$("#id_is_editClub").val("");
		$(".dataClub").val('');
	})
	$('#location').on('change', function() {
	 	 if($(this).val() == "") {
	        $("#"+$(this).attr("id_tmp")).text("this field is required");
	        $("#"+$(this).attr("id_tmp")).removeClass("displayNONE");
	        $("#"+$(this).attr("id_tmp")).parent().removeClass("displayNONE");
	    }
	    else {
	        $("#"+$(this).attr("id_tmp")).addClass("displayNONE");
	        $("#"+$(this).attr("id_tmp")).parent().addClass("displayNONE");
	    }
	});
	$("#club_save").click(function() {
		id_editClub = $("#id_is_editClub").val();
		json_obj = $("#updateClub").serializeArray();
		var check = true;

		for (var key in json_obj) {
			if(json_obj[key]['value'] == "" && json_obj[key]['name'] != 'lat' && json_obj[key]['name'] != 'lng' && json_obj[key]['name'] != 'age_restriction' && json_obj[key]['name'] != 'content' && key != 0) {
				$("#"+$(".dataClub[name="+json_obj[key]['name']+"]").attr("id_tmp")).removeClass("displayNONE");
				$("#"+$(".dataClub[name="+json_obj[key]['name']+"]").attr("id_tmp")).parent().removeClass("displayNONE");
				check = false;
			}
		}
		if($("#websiteClub").attr('valid') == 'false') {
			check = false;
		}
		if(check) {
			var url = "/posts/updateClub/";     
	        $.ajax({
	          type: "POST",
	          url: url,
	          data: $("#updateClub").serialize(),
	        }).done(function(club_id){
				$('#modalEditClub').modal("hide");
				if (id_editClub != '') {
					var i = 0;
					for (var key in json_obj) {
						
						if(key <= $(".club"+idClub).children().length - 1 && key != 0){
							if (json_obj[key]['value'] == 'l') {
								$(".club"+idClub+"").children().eq(i).text('18-22');
							}
							else if (json_obj[key]['value'] == 'm') {
								$(".club"+idClub+"").children().eq(i).text('23-27');
							}
							else if (json_obj[key]['value'] == 'h') {
								$(".club"+idClub+"").children().eq(i).text('28-32');
							}
							else if (json_obj[key]['value'] == '' && json_obj[key]['name'] == 'age_restriction' ) {
									$(".club"+club_id+"").children().eq(i).text('*');
							}
							else {
								$(".club"+idClub).children().eq(i).text(json_obj[key]['value']);
		        				
							}
							i++;
		        		}
					}
				}
				else {
					var html =  '<tr class="club'+club_id+'"><td >{{inf.title}}</td><td>{{inf.address}}</td><td>{{inf.phone_number}}</td><td>{{inf.hours_of_operation}}</td><td>{{inf.cover_price}}</td><td>{{inf.dress_code}}</td><td>{{inf.capacity}}</td><td>{{inf.age_restriction}}</td><td>{{inf.website|safe}}</td><td>{{inf.type_of_music}}</td><td idClub="'+club_id+'"><a class="glyphicon glyphicon-eye-open btnActionClub btnWatch" href="/business/'+club_id+'"></a><span class="glyphicon glyphicon-pencil btnActionClub btnEditClub" data-toggle="modal" data-target="#modalEditClub"></span><span class="glyphicon glyphicon-trash btnActionClub btnDeleteClub" data-toggle="modal" data-target="#modalDelClub"></span></td></tr>';
					$("#allClub").append(html);
					var i = 0;
					for (var key in json_obj) {
							if(key <= $(".club"+club_id+"").children().length - 1 && key != 0){
								if (json_obj[key]['value'] == 'l') {
									$(".club"+club_id+"").children().eq(i).text('18-22');
								}
								else if (json_obj[key]['value'] == 'm') {
									$(".club"+club_id+"").children().eq(i).text('23-27');
								}
								else if (json_obj[key]['value'] == 'h') {
									$(".club"+club_id+"").children().eq(i).text('28-32');
								}
								else if (json_obj[key]['value'] == '' && json_obj[key]['name'] == 'age_restriction' ) {
									$(".club"+club_id+"").children().eq(i).text('*');
								}
								else {
									$(".club"+club_id+"").children().eq(i).text(json_obj[key]['value']);
								}
			        			
			        			i++;
			        		}
					}

	        	}
	        });
		}

    });
	$(".listClubs").on("click", ".btnDeleteClub", function() {
		idClub = $(this).parent().attr("idClub");
	})
	$("#btnDeleteClub").click(function() {
		var url = "/posts/deleteClub/";     
        $.ajax({
          type: "POST",
          url: url,
          data: {idClub:idClub},
        }).done(function(response){
			$('#modalDelClub').modal("hide");
			$(".club"+idClub).remove();
        });
		
	})
	function validate1(link) {
	  var rg = /^(([a-zA-Z0-9$\-_.+!*'(),;:&=]|%[0-9a-fA-F]{2})+@)?(((25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])){3})|localhost|([a-zA-Z0-9\-\u00C0-\u017F]+\.)+([a-zA-Z]{2,}))(:[0-9]+)?(\/(([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*(\/([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*)*)?(\?([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?(\#([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?)?$/;
	  return rg.test(link);
	}
	function validate2(link) {
		var rg = /^(http|https):\/\/(([a-zA-Z0-9$\-_.+!*'(),;:&=]|%[0-9a-fA-F]{2})+@)?(((25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])){3})|localhost|([a-zA-Z0-9\-\u00C0-\u017F]+\.)+([a-zA-Z]{2,}))(:[0-9]+)?(\/(([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*(\/([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*)*)?(\?([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?(\#([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?)?$/;
		return rg.test(link);
	}

	$("#websiteClub").blur(function() {
		if(validate1($("#websiteClub").val()) || validate2($("#websiteClub").val())) {
			$("#"+$(this).attr("id_tmp")).addClass("displayNONE");
	        $("#"+$(this).attr("id_tmp")).parent().addClass("displayNONE");
	        $(this).attr("valid","true");
		}
		else {
			$("#"+$(this).attr("id_tmp")).text("Enter a valid URL.");
			$("#"+$(this).attr("id_tmp")).removeClass("displayNONE");
	        $("#"+$(this).attr("id_tmp")).parent().removeClass("displayNONE");
			$(this).attr("valid","false");
		}
	})

})