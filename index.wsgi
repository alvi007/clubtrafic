import os
import sys
import site
site.addsitedir('~/.virtualenvs/clubtraficenv/local/lib/python2.7/site-packages')
sys.path.append('/home/ubuntu/webapps/clubtrafic')
sys.path.append('/home/ubuntu/webapps/clubtrafic/club_traffic')
os.environ['DJANGO_SETTINGS_MODULE'] = 'club_traffic.settings'
activate_env = os.path.expanduser("/home/ubuntu/.virtualenvs/clubtraficenv/bin/activate_this.py")
execfile(activate_env , dict(__file__ = activate_env))
import django.core.wsgi
application = django.core.wsgi.get_wsgi_application()
